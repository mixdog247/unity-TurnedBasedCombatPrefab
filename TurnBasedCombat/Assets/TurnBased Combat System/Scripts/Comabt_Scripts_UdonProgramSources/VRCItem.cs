﻿
using UdonSharp;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;


#endif

[UdonBehaviourSyncMode(BehaviourSyncMode.None)]

public class VRCItem : UdonSharpBehaviour
{
    #region EditorOnly
#if UNITY_EDITOR
    public enum ItemType
    {
        KeyItem,
        Consumeable,
        PermenantItem
    }

    public ItemType itemType;
#endif
    #endregion EditorOnly
    [SerializeField] string ItemTypeString;
    #region KeyItemVariables
    [SerializeField] bool Aquired = false;
    #endregion KeyItemVariables

    #region ConsumeableVariables
    [SerializeField] int HealingAmount;
    [SerializeField] int DamageMultiplyer;
    [SerializeField] int AbsorptionMultiplyer;
    [SerializeField] int EnemyAccuracyMultiplyer;
    [SerializeField] int EnemyStunAmount;
    #endregion ConsumeableVariables

    #region PermenantItemVariables
    [SerializeField] int MaxHealthIncrease;
    [SerializeField] int MaxMannaIncrease;
    [SerializeField] int HealthRegenRateIncrease;
    [SerializeField] int MannaRegenRateIncrease;
    #endregion PermenantItemVariables

}

[CustomEditor(typeof(VRCItem))]
public class VRCItemEditor : Editor
{
    private VRCItem item;

    private SerializedProperty itemTypeStringProp;

    private SerializedProperty itemTypeProp;
    private SerializedProperty healingAmountProp;
    private SerializedProperty damageBuffAmountProp;
    private SerializedProperty shieldBuffAmountProp;

    private void OnEnable()
    {
        item = (VRCItem)target;
        itemTypeProp = serializedObject.FindProperty("itemType");
        healingAmountProp = serializedObject.FindProperty("healingAmount");
        damageBuffAmountProp = serializedObject.FindProperty("damageBuffAmount");
        shieldBuffAmountProp = serializedObject.FindProperty("shieldBuffAmount");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(itemTypeProp);

        switch (item.itemType)
        {
            case VRCItem.ItemType.KeyItem:
                EditorGUILayout.PropertyField(healingAmountProp);
                break;
            case VRCItem.ItemType.Consumeable:
                EditorGUILayout.PropertyField(damageBuffAmountProp);
                break;
            case VRCItem.ItemType.PermenantItem:
                EditorGUILayout.PropertyField(shieldBuffAmountProp);
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
