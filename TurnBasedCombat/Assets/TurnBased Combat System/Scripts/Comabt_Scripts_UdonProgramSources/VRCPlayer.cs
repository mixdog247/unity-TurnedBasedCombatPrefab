﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

[UdonBehaviourSyncMode(BehaviourSyncMode.None)]
public class VRCPlayer : UdonSharpBehaviour
{
    [SerializeField] int MaxPlayerHealth;
    [SerializeField] int MaxPlayerManna;
    [SerializeField] int PlayerHealthRegen;
    [SerializeField] int PlayerManaRegen;
}
