﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
#if UNITY_EDITOR
using UnityEditor;
#endif 

[UdonBehaviourSyncMode(BehaviourSyncMode.Manual)]
public class TurnBasedCombatSystem : UdonSharpBehaviour
{
    [Header("GameStateVariables")]
    [SerializeField] string CombatState = "Inactive";

    [Header("Enemy Variables")]
    [SerializeField] VRCEnemy EnemyInstance;

    [Header("Player Variables")]
    [SerializeField] VRCPlayer PlayerInstace;

    [Header("RunTimeVariables")]
    [SerializeField] int CurrentEnemyHealth;
    [SerializeField] int CurrentPlayerHealth;





}


[CustomEditor(typeof(TurnBasedCombatSystem))]
public class TurnBasedCombatSystemEditor : Editor 
{

}